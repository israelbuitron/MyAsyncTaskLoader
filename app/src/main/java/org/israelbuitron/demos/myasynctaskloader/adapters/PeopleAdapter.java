package org.israelbuitron.demos.myasynctaskloader.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.israelbuitron.demos.myasynctaskloader.R;
import org.israelbuitron.demos.myasynctaskloader.beans.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * @author israel.buitron
 */
public class PeopleAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Person> mPeople;
    private TextView mIdView;
    private TextView mNameView;

    public PeopleAdapter(Context context) {
        mPeople = new ArrayList<Person>();
        mInflater = LayoutInflater.from(context);
    }

    public PeopleAdapter(Context context, List<Person> people) {
        mPeople = people;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Get person object
        Person emp = (Person) getItem(position);

        if (view == null) {
            view = mInflater.inflate(R.layout.person, null);
        }

        // Get views from layout
        mIdView = (TextView) view.findViewById(R.id.person_id_view);
        mNameView = (TextView) view.findViewById(R.id.person_name_view);

        // Fill views
        mIdView.setText(emp.getId());
        mNameView.setText(emp.getName());

        return view;
    }

    @Override
    public Object getItem(int position) {
        return mPeople.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mPeople.size();
    }

    public void setPeople(List<Person> data) {
        mPeople.addAll(data);
        notifyDataSetChanged();
    }
}
